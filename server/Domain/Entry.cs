﻿namespace PictureChallenge.Domain;
public record Entry
{
    public Guid Id { get; }
    public DateTime Date { get; }
    public User User { get; }
    public Uri Url { get; }

    public Entry(DateTime date, User user, string url)
    {
        Id = new Guid();
        Date = date;
        User = user;
        Url = new Uri(url);
    }
    public Entry(DateTime date, User user, Uri url)
    {
        Id = new Guid();
        Date = date;
        User = user;
        Url = url;
    }
    
}