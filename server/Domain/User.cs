﻿namespace PictureChallenge.Domain;

public record User(string Name, Guid Id)
{

    public User(string name) : this(name,Guid.NewGuid()) 
    {
    }
}