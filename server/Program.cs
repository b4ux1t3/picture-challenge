using PictureChallenge.Startup;

// Configure our application's services.
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddServices();

// Build and get our API ready.
var app = builder.Build();
app.AddRoutes();
app.UseCors();

var folder = Environment.SpecialFolder.LocalApplicationData;
var path = Environment.GetFolderPath(folder);

app.Run();
