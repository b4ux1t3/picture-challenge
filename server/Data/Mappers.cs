﻿using PictureChallenge.API.Downstream.Dto;
using PictureChallenge.Data.Entities;
using PictureChallenge.Domain;

namespace PictureChallenge.Data;

public static class Mappers
{
    public static Entry ToDomain(this EntryEntity? entity) => 
        entity is null ?
            new (DateTime.MinValue, new("blank"), new Uri("")):
            new(entity.Date, entity.User.ToDomain(), entity.Url);
    public static User ToDomain(this UserEntity? entity) => 
        entity is null ? 
            new("blank") :
            new(entity.Name, entity.Id);

    public static EntryEntity ToEntity(this Entry entry) =>
        new(entry.Id, entry.Date, entry.User.ToEntity(), entry.Url);
    public static UserEntity ToEntity(this User user) =>
        new(user.Name, user.Id);

    public static ClientSideEntry ToClient(this Entry entry) =>
        new(entry.Date, entry.User.Name, entry.Url);
}