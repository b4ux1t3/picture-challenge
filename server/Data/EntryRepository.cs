﻿using System.Data;
using MongoDB.Bson;
using MongoDB.Driver;
using PictureChallenge.Data.Entities;
using PictureChallenge.Domain;

namespace PictureChallenge.Data;

public interface IEntryRepository
{
    Task<Entry> Find(Entry entry);
    Task<User> Find(User user);
    Task<Entry> Add(Entry entry);
    Task<User> Add(User user);
    Task<Entry> Update(Entry entry);
    Task<User> Update(User user);
    Task<Entry> Save(Entry entry);
    Task<User> Save(User user);
    Task Remove(Entry entry);
    Task Remove(User user);
    Task<IEnumerable<Entry>> GetAllEntries();
    Task<IEnumerable<User>> GetAllUsers();
    Task<IEnumerable<Entry>> EntriesForUser(User user);

}

public class EntryRepository : IEntryRepository
{
    private readonly IMongoDatabase _db;
    
    public EntryRepository(IConfiguration config)
    {
        var client = new MongoClient(config.GetConnectionString("DefaultConnection"));
        _db = client.GetDatabase("PictureChallenge");
    }

    public async Task<Entry> Find(Entry entry)
    {
        throw new NotImplementedException();

    }
    public async Task<User> Find(User user)
    {
        throw new NotImplementedException();

    }

    public async Task Remove(User user)
    {
        throw new NotImplementedException();
    }

    public async Task Remove(Entry entry)
    {
        throw new NotImplementedException();
    }

    public async Task<Entry> Update(Entry entry)
    {
        throw new NotImplementedException();
    }
    
    public async Task<User> Update(User user)
    {
        throw new NotImplementedException();
    }

    public async Task<Entry> Save(Entry entry)
    {
        throw new NotImplementedException();
    }
    public async Task<User> Save(User user)
    {
        throw new NotImplementedException();
    }

    public async Task<IEnumerable<Entry>> GetAllEntries()
    {
        var collection = _db.GetCollection<EntryEntity>("Entries");
        var documents = await collection.FindAsync(new BsonDocument());
        var list = await documents.ToListAsync();
        var result = list.Select(entity => entity.ToDomain());

        return result;
    }

    public async Task<IEnumerable<User>> GetAllUsers()
    {
        var collection = _db.GetCollection<UserEntity>("Users");
        var documents = await collection.FindAsync(new BsonDocument());
        var list = await documents.ToListAsync();
        var result = list.Select(entity => entity.ToDomain());

        return result;
    }

    public async Task<Entry> Add(Entry entry)
    {
        var collection = _db.GetCollection<EntryEntity>("Entries");
        await collection.InsertOneAsync(entry.ToEntity());
        return entry;
    }

    public async Task<User> Add(User user)
    {
        var collection = _db.GetCollection<UserEntity>("Users");
        await collection.InsertOneAsync(user.ToEntity());
        return user;
    }
    public async Task<IEnumerable<Entry>> EntriesForUser(User user)
    {
        var collection = _db.GetCollection<EntryEntity>("Entries");

        var filter = Builders<EntryEntity>.Filter.Gt("User.Id", user.Id);
        var cursor = await collection.Find(filter).ToCursorAsync();
        var entries = await cursor.ToListAsync();
        return entries.Select(entry => entry.ToDomain());
    }


}
