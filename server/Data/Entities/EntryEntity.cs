﻿namespace PictureChallenge.Data.Entities;

public record EntryEntity( Guid Id, DateTime Date, UserEntity User, Uri Url);