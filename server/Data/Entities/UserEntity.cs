﻿namespace PictureChallenge.Data.Entities;

public record UserEntity(string Name, Guid Id)
{

    public UserEntity(string name) : this(name, Guid.NewGuid())
    {
        
    }
}