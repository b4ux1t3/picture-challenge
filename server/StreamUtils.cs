﻿using System.Text;

namespace PictureChallenge;

public static class StreamUtils
{
    public static void ReadAllBytes(this Stream stream, StringBuilder sb)
    {
        var nextByte = stream.ReadByte();
        while (nextByte != -1)
        {
            sb.Append((char) nextByte);
            nextByte = stream.ReadByte();
        }
    }
    public static void ReadAllBytes(this Stream stream, List<byte> list)
    {
        var nextByte = stream.ReadByte();
        while (nextByte != -1)
        {
            list.Add((byte) nextByte);
            nextByte = stream.ReadByte();
        }
    }
}