﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using PictureChallenge.API.Downstream;
using PictureChallenge.API.Upstream;
using PictureChallenge.Data;

namespace PictureChallenge.Startup;

public static class ServiceMapping
{
    public static IServiceCollection AddServices(this IServiceCollection services)
    {
        services.AddCors(options => options.AddPolicy("AnyOrigin", o => o.AllowAnyOrigin()));
        services.AddHttpClient();
        services.TryAddSingleton<IImgurApi, ImgurApi>();
        services.TryAddSingleton<IEntryRepository, EntryRepository>();
        services.TryAddSingleton<ISubmissionRouteHandler, SubmissionRouteHandler>();

        return services; 
    }
}
