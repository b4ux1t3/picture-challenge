﻿using PictureChallenge.API.Downstream;
using PictureChallenge.Data;

namespace PictureChallenge.Startup;

public static class RouteMapping
{
    public static WebApplication AddRoutes(this WebApplication app)
    {
        var submissionHandler = app.Services.GetRequiredService<ISubmissionRouteHandler>();
        app.MapGet("/", () => "Hello World!");
        app.MapGet("/images", async (HttpRequest req) =>
        {
            var repo = app.Services.GetRequiredService<IEntryRepository>();
            var entries = await repo.GetAllEntries();
            app.Logger.LogInformation($"{req.HttpContext.Connection.RemoteIpAddress}: {req.Path.Value}");
            return Results.Json(entries.Select(entry => entry.ToClient()));
        }).RequireCors("AnyOrigin");
        app.MapPost("/submit", submissionHandler.SubmitPicture).Accepts<IFormFile>("multipart/form-data");

        return app;
    }
}
