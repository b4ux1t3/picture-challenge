﻿using PictureChallenge.API.Upstream;
using PictureChallenge.Data;
using PictureChallenge.Domain;

namespace PictureChallenge.API.Downstream;

public interface ISubmissionRouteHandler
{
    Task<IResult> SubmitPicture(HttpRequest req);
}
public class SubmissionRouteHandler: ISubmissionRouteHandler
{
    private readonly IImgurApi _api;
    private readonly ILogger<SubmissionRouteHandler> _logger;
    private readonly IEntryRepository _entryRepository;
    public SubmissionRouteHandler(
        IImgurApi api, 
        ILogger<SubmissionRouteHandler> logger,
        IEntryRepository entryRepository
    )
    {
        _api = api;
        _logger = logger;
        _entryRepository = entryRepository;
    }
    
    public async Task<IResult> SubmitPicture(HttpRequest req)
    {
        if (!req.HasFormContentType) return Results.BadRequest();
    
        var form = await req.ReadFormAsync();
        var file = form.Files["file"];
        if (file is null) return Results.BadRequest();
        var fileBytes = new List<byte>();
        await using var fileStream = file.OpenReadStream();
        fileStream.ReadAllBytes(fileBytes);

        Uri? newUploadUri;

        try
        {
            newUploadUri = await _api.UploadPicture(file.Name, fileBytes);
        }
        catch (InvalidDataException)
        {
            return Results.BadRequest();
        }
        catch (Exception)
        {
            return Results.StatusCode(503);
        }
    
        var result = await _entryRepository.Add(new Entry(DateTime.Now, new User(form["name"].ToString()), newUploadUri));
        
        _logger.LogInformation($"New image for user {form["name"]}: {newUploadUri}");
        return Results.NoContent();

    }
}
