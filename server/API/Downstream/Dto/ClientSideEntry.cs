﻿namespace PictureChallenge.API.Downstream.Dto;

public record ClientSideEntry(DateTime Date, string User, Uri Url);