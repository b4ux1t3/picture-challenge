﻿namespace PictureChallenge.API.Downstream.Dto;

public record EntrySubmission(Guid Id, string Name);
