﻿using System.Text.Json.Serialization;

namespace PictureChallenge.PictureChallenge.API.Upstream.Dto;

public record AccessTokenResponse
{
    [JsonPropertyName("access_token")] public string AccessToken = "";
    [JsonPropertyName("expires_in")] public long ExpiresIn = 0;
    [JsonPropertyName("token_type")] public string TokenType = "";
    [JsonPropertyName("scope")] public string Scope = "";
    [JsonPropertyName("refresh_token")] public string RefreshToken = "";
    [JsonPropertyName("account_id")] public long AccountId = 0;
    [JsonPropertyName("account_username")] public string AccountUsername = "";
}
