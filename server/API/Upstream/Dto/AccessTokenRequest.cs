﻿using System.Text.Json.Serialization;

namespace PictureChallenge.API.Upstream.Dto;


internal record AccessTokenRequest
{
    [JsonPropertyName("refresh_token")] public string RefreshToken { get; set; } = "";
    [JsonPropertyName("client_id")] public string ClientId { get; set; } = "";
    [JsonPropertyName("client_secret")] public string ClientSecret { get; set; } = "";
    [JsonPropertyName("grant_type")] public string GrantType => "refresh_token";
}