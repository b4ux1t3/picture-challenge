﻿using System.Text.Json.Serialization;

namespace PictureChallenge.API.Upstream.Dto;

public record ImageUploadResponse()
{
    [JsonPropertyName("data")] public Data? data { get; init; }
    [JsonPropertyName("success")] public bool Success { get; init; }
    [JsonPropertyName("status")] public int Status { get; init; }
    
    public record Data
    {
        [JsonPropertyName("id")] public string? Id { get; init; }
        [JsonPropertyName("title")] public object? Title { get; init; }
        [JsonPropertyName("description")] public object? Description { get; init; }
        [JsonPropertyName("datetime")] public int? Datetime { get; init; }
        [JsonPropertyName("type")] public string? Type { get; init; }
        [JsonPropertyName("animated")] public bool? Animated { get; init; }
        [JsonPropertyName("width")] public int? Width { get; init; }
        [JsonPropertyName("height")] public int? Height { get; init; }
        [JsonPropertyName("size")] public int? Size { get; init; }
        [JsonPropertyName("views")] public int? Views { get; init; }
        [JsonPropertyName("bandwidth")] public int? Bandwidth { get; init; }
        [JsonPropertyName("vote")] public object? Vote { get; init; }
        [JsonPropertyName("favorite")] public bool? Favorite { get; init; }
        [JsonPropertyName("nsfw")] public object? Nsfw { get; init; }
        [JsonPropertyName("section")] public object? Section { get; init; }
        [JsonPropertyName("account_url")] public object? AccountUrl { get; init; }
        [JsonPropertyName("account_id")] public int? AccountId { get; init; }
        [JsonPropertyName("is_ad")] public bool? IsAd { get; init; }
        [JsonPropertyName("in_most_viral")] public bool? InMostViral { get; init; }
        [JsonPropertyName("tags")] public List<object> Tags { get; init; } = new();
        [JsonPropertyName("ad_type")] public int? AdType { get; init; }
        [JsonPropertyName("ad_url")] public string? AdUrl { get; init; }
        [JsonPropertyName("in_gallery")] public bool? InGallery { get; init; }
        [JsonPropertyName("deletehash")] public string? DeleteHash { get; init; }
        [JsonPropertyName("name")] public string? Name { get; init; }
        [JsonPropertyName("link")] public string? Link { get; init; }
    }
        
};