﻿namespace PictureChallenge.API.Upstream;
public class AccessToken
{
    public string Token { get; }
    public DateTime ExpirationDate { get; }

    public AccessToken(string tokenString, DateTime expirationDate)
    {
        Token = tokenString;
        ExpirationDate = expirationDate;
    }
}