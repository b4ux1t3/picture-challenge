﻿namespace PictureChallenge.API.Upstream;
public record ImgurInfo(string UploadUrl, string TokenUrl, string CId, string CSecret, string RefreshToken);