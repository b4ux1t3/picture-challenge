﻿using System.Net.Http.Headers;
using System.Text.Json;
using PictureChallenge.API.Upstream.Dto;
using PictureChallenge.PictureChallenge.API.Upstream.Dto;

namespace PictureChallenge.API.Upstream;

public interface IImgurApi
{
    Task<Uri> UploadPicture(string fileName, IReadOnlyList<byte> bytes);

}

public class ImgurApi : IImgurApi
{
    private readonly HttpClient _http;
    private readonly ImgurInfo _info;
    private readonly ILogger<ImgurApi> _logger;
    private AccessToken? _accessToken;
    
    public ImgurApi(
        HttpClient http, 
        ILogger<ImgurApi> logger,
        IConfiguration config
        )
    {
        _http = http;
        _logger = logger;
        var imgurConfig = config.GetSection("Imgur");
        if (imgurConfig is null) throw new Exception("Could not locate Imgur configuration section.");
        _info = new ImgurInfo(
            imgurConfig["image-url"],
            imgurConfig["token-url"],
            imgurConfig["client-id"], 
            imgurConfig["client-secret"],
            imgurConfig["refresh-token"]
        );
        _http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Client-ID", _info.CId);
        GetAccessToken();
    }


    private async Task GetAccessToken()
    {
        var tokenResponse = await _http.GetFromJsonAsync<AccessTokenResponse>(_info.TokenUrl);

        if (tokenResponse is null) throw new Exception($"Could not retrieve an access token. Check that the refresh token is valid.");
        var expiryDate = DateTime.Now.Add(new TimeSpan(tokenResponse.ExpiresIn));
        _accessToken = new AccessToken(tokenResponse.AccessToken, expiryDate);
        _http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _accessToken.Token);
    }

    public async Task<Uri> UploadPicture(string fileName, IReadOnlyList<byte> bytes)
    {
        var sentData = new MultipartFormDataContent();
        var values = new List<KeyValuePair<string, string>>
        {
            new ("type", "file"),
            new ("title", fileName)
        };
        
        sentData.Add( new ByteArrayContent(bytes.ToArray(), 0, bytes.Count), "image", fileName);
        sentData.Add(new FormUrlEncodedContent(values));

        var response = await _http.PostAsync(_info.UploadUrl, sentData);
        
        if (!response.IsSuccessStatusCode)
        {
            _logger.LogInformation($"Error response from Imgur: {response.StatusCode}");
            throw new InvalidDataException("Received an error from Imgur when uploading an image.");
        }

        var parsedResponse = JsonSerializer.Deserialize<ImageUploadResponse>(await response.Content.ReadAsStreamAsync());

        return new Uri(parsedResponse?.data?.Link ?? throw new Exception("Did not receive a valid response from imgur when uploading a file."));
    }
}