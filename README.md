
# PictureChallenge

Just a little challenge for my wife and I. Feel free to clone and run your own challenge!

Instead of using, you know, proper authentication, I'm hiding the actual website behind the GitLab Pages for the project, which aren't accessible unless you're a member of the repo.

Since the client and the server are completely deparate from each other, you can put the included client behind whatever security you want.

In the end, the application is basically a shim for Imgur's API, so it could, theorietically, be consumed by any front-end implementing whatever challenge you wanted!

## Configuration
There needs to be an `appsettings.json` file next to the executable with the following layout:

``` JSON
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "ConnectionStrings": {
    "DefaultConnection": "Host=<POSTGRES HOST>;Database=PictureChallenge;Username=postgres;Password=<DB_PASSWORD_HERE>;"
  },
  "Imgur": {
    "image-url": "https://api.imgur.com/3/upload",
    "token-url": "https://api.imgur.com/oauth2/token",
    "client-id": "<CLIENT_ID>",
    "client-secret": "<CLIENT_SECRET>",
    "refresh-token": "<REFRESH_TOKEN>"
  },
  "AllowedHosts": "*"
}
```


## Database
When running in Docker Compose, you need to make sure you have the `PICTURE_CHALLENGE_DB_PASSWORD` environment variable set. The password you use in your `appsettings.json` file should match this password.

Then, you're going to have to run our "migrations" (ha!) manually by connecting to the container using something like PGAdmin or your IDE's build-in database connector, then executing the the migration files in `./database/migrations`. This won't ever be a super complicated schema, so I doubt it will take a DBA to get the DB up and running. _If you open up issue against this project asking me to debug Postgres, I will laugh at you and close the issue._ I'm not a DBA.