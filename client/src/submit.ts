import { DEPLOY_URL } from "../environment/environment";

const nameInput = document.querySelector("#inp-name") as HTMLInputElement;
const fileInput = document.querySelector("#inp-image") as HTMLInputElement;
const preview = document.querySelector("#img-submission") as HTMLImageElement;
const submitButton = document.querySelector("#btn-submit") as HTMLButtonElement;

fileInput.addEventListener('change', (e: Event) => {
  console.log((e.target as HTMLInputElement).files);
  const file = (e.target as HTMLInputElement).files[0];
  if (file.type.split('/')[0] === 'image') {
    preview.src = URL.createObjectURL(file);
    preview.style.display="block";
  }
});

submitButton.addEventListener('click', submit);

async function submit(): Promise<void> {
  if (nameInput.value.length < 1) {
    console.log("No name!");
    return;
  }
  const data = new FormData();
  data.append('file', fileInput.files[0]);
  data.append('name', nameInput.value);

  const response = await fetch(`${DEPLOY_URL}/submit`, {
    method: 'POST',
    mode: 'no-cors',
    body: data,
  });

  if (response.ok){
    nameInput.disabled = true;
    fileInput.disabled = true;
    submitButton.disabled = true;

  }
}
