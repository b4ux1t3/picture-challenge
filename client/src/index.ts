import { DEPLOY_URL } from "../environment/environment";
const monthLookup = ['Janurary','February','March','April','May','June','July','August','September','October','November','December'];

const mainContainer = document.querySelector('#main') as HTMLDivElement;

async function getEntries() {
  const entries = await fetch(`${DEPLOY_URL}/images`);
  return entries;
}

async function main(){
  const entries: RawEntry[] = await getEntries().then(data => data.json());
  const parsedEntries: Entry[] = entries.map(entry => new Entry(entry.date, entry.user, entry.url));

  const today = new Date();
  let day = today.getDate();

  do {
    const currentDay = new Date(today.getFullYear(), today.getMonth(), day)
    const entriesForDate = getEntriesFromDate(parsedEntries, currentDay);
    const newChild = entriesForDate.length > 0
      ? generateEntryDivs(entriesForDate)
      : generateBlankDiv(currentDay);
    mainContainer.appendChild(newChild);
    mainContainer.appendChild(document.createElement('hr'));
  } while (day-- > 1)
}

function generateEntry(entry: Entry): HTMLDivElement {
  const newEntry = document.createElement('div');
  newEntry.classList.add('entry', 'col-sm-6');
  const image = document.createElement('img');
  image.classList.add('entry-img', 'img-fluid');
  const header = document.createElement('h3');
  header.innerText = entry.user;
  image.src = entry.url;
  newEntry.appendChild(header);
  newEntry.appendChild(image);
  return newEntry;
}

function getEntriesFromDate(entries: Entry[], date: Date): Entry[] {
  return entries.filter(entry => {
    return entry.date.getMonth() === date.getMonth() && entry.date.getDate() === date.getDate();
  })
}

function generateBlankDiv(date: Date): HTMLDivElement {
  const today = new Date();
  const isToday = date.getDate() === today.getDate() && date.getMonth() === today.getMonth()

  const newDiv = document.createElement('div');
  newDiv.classList.add('day', 'row');

  const newHeader = document.createElement('h2');
  newHeader.innerText = `${monthLookup[date.getMonth()]} ${date.getDate()}`;
  if (isToday) newHeader.innerText += " (Today)";

  newDiv.appendChild(newHeader);

  const newPara = document.createElement('p');

  newPara.innerText = isToday
    ? `No entries yet!`
    : `No entries on ${newHeader.innerText}. 😞`;

  newDiv.appendChild(newPara);

  return newDiv;
}

function generateEntryDivs(entries: Entry[]): HTMLDivElement {
  const today = new Date();
  const date = entries[0].date
  const isToday = date.getDate() === today.getDate() && date.getMonth() == today.getMonth();

  const newEntryDivs: HTMLDivElement[] = entries.map(entry => generateEntry(entry));

  const newDiv = document.createElement('div');
  newDiv.classList.add('day', 'row');

  const newHeader = document.createElement('h2');
  newHeader.innerText = `${monthLookup[date.getMonth()]} ${date.getDate()}`;
  if (isToday) newHeader.innerText += " (Today)";

  newDiv.appendChild(newHeader);
  newEntryDivs.forEach(div => newDiv.appendChild(div));
  return newDiv;
}

main();



class Entry{
  public date: Date;
  constructor(date: string, public user: string, public url: string) {
    this.date = new Date(date);
  }
}

class RawEntry {
  public date: string;
  public user: string;
  public url: string;
}
